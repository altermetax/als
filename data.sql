/* Nota: il campo id non viene creato perche´ in SQLite3 esiste una colonna di default di nome ROWID */
CREATE TABLE routes(
path VARCHAR(255),
verb VARCHAR(10),
controller VARCHAR(50),
action VARCHAR(50)
);